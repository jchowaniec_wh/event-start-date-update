package com.williamhill;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.williamhill.selenium.BackOffice;
import com.williamhill.services.gtp.service.GtpService;
import com.williamhill.services.openbet.service.OxiFeedService;
import com.williamhill.services.pds.service.PdsService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static java.util.Objects.isNull;

@Slf4j
public class App {
    public static void main(String[] args) throws JsonProcessingException {

        List<String> eventIds = getAllEventsFromOpenBet();
        ALL_EVENTS_TO_CHECK = eventIds.size();
        updateEventStartDateTimeForValidEvents(eventIds);
    }

    private static int ALL_EVENTS_TO_CHECK;

    private static int UPDATED_EVENTS = 0;

    private static List<String> getAllEventsFromOpenBet() {
        return new BackOffice()
                .open()
                .getAllEventIds();
    }

    private static void updateEventStartDateTimeForValidEvents(List<String> eventIds) throws JsonProcessingException {
        for (String eventId : eventIds) {
            logHowManyEventsLeftToCheck();
            String eventGtpId = new PdsService().getGtpId(eventId);

            if (isNull(eventGtpId)) {
                log.info("No required event data id PDS ....");
                continue;
            }

            if (!new OxiFeedService().isGtpIdExistsInOpenBet(eventGtpId)) {
                log.info("No Gtp id in Open Bet ....");
                continue;
            }

            new GtpService().updateEventStartDateTime(eventGtpId);
            logHowManyEventsWereUpdated();
        }
        logFinalNumberOfUpdatedEvents();
    }

    private static void logHowManyEventsLeftToCheck() {
        log.info("=======================================================");
        log.info("Events that still need to be checked... " + ALL_EVENTS_TO_CHECK);
        log.info("=======================================================");
        ALL_EVENTS_TO_CHECK = ALL_EVENTS_TO_CHECK - 1;
    }

    private static void logHowManyEventsWereUpdated() {
        UPDATED_EVENTS = UPDATED_EVENTS + 1;
        logUpdatedEvents();
    }

    private static void logFinalNumberOfUpdatedEvents() {
        logUpdatedEvents();
    }

    private static void logUpdatedEvents() {
        log.info("=======================================================");
        log.info("updatedEvents " + UPDATED_EVENTS);
        log.info("=======================================================");
    }

}
