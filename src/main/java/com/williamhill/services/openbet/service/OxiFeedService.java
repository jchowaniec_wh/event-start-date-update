package com.williamhill.services.openbet.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.williamhill.services.openbet.contract.OxiFeedRequest;
import com.williamhill.services.openbet.contract.auth.Auth;
import com.williamhill.services.openbet.contract.event.Event;
import com.williamhill.services.openbet.utils.SoapService;
import com.williamhill.utils.PropertiesManager;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OxiFeedService extends SoapService {

    public Boolean isGtpIdExistsInOpenBet(String eventGtpId) throws JsonProcessingException {
        log.info("Check is GTP ID exists in Open Bet...");

        OxiFeedRequest oxiFeedRequest = OxiFeedRequest
                .builder()
                .auth(getAuth())
                .event(getEvent(eventGtpId))
                .build();

        return !post(oxiFeedRequest)
                .prettyPrint()
                .contains("No OpenBet id found for this external ID");
    }

    private Auth getAuth() {
        return Auth
                .builder()
                .username(PropertiesManager.getOxiFeedUsername())
                .password(PropertiesManager.getOxiFeedPassword())
                .build();
    }

    private Event getEvent(String eventGtpId) {
        return Event.builder()
                .externalId(eventGtpId)
                .build();
    }

}