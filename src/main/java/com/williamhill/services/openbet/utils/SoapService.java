package com.williamhill.services.openbet.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import com.williamhill.utils.PropertiesManager;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class SoapService {

    private final XmlMapper xmlMapper;

    public SoapService() {
        xmlMapper = new XmlMapper();
        xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
    }

    public Response post(Object body) throws JsonProcessingException {
        return given()
                .log().all()
                .contentType(ContentType.XML)
                .body(getMappedXmlString(body))
                .post(PropertiesManager.getOxiFeedEndpoint());
    }

    private String getMappedXmlString(Object value) throws JsonProcessingException {
        return xmlMapper.writeValueAsString(value);
    }

}
