package com.williamhill.services.openbet.contract.event;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class Event {

    @JacksonXmlProperty(isAttribute = true)
    private String externalId;

}
