package com.williamhill.services.openbet.contract;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.williamhill.services.openbet.contract.auth.Auth;
import com.williamhill.services.openbet.contract.event.Event;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@JacksonXmlRootElement(localName = "oxiFeedRequest")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class OxiFeedRequest {

    private Auth auth;

    private Event event;

}
