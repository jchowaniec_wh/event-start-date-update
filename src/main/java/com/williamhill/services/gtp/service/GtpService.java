package com.williamhill.services.gtp.service;

import com.williamhill.services.gtp.contract.UpdateEventStartTime;
import com.williamhill.utils.PropertiesManager;
import com.williamhill.utils.Time;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.given;

public class GtpService {

    public void updateEventStartDateTime(String eventGtpId) {
        UpdateEventStartTime updateEventStartTime = UpdateEventStartTime
                .builder()
                .eventId(eventGtpId)
                .startTime(Time.getRandomTodayDate())
                .build();

        try {
            given()
                    .log().all()
                    .contentType(ContentType.JSON)
                    .body(updateEventStartTime)
                    .post(PropertiesManager.getGtpUrl())
                    .prettyPrint();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
