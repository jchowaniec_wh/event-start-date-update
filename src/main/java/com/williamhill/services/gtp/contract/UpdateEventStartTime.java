package com.williamhill.services.gtp.contract;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class UpdateEventStartTime {

    private String eventId;

    private String startTime;

}
