package com.williamhill.services.pds.service;

import com.williamhill.services.pds.contract.EventInPds;
import com.williamhill.utils.PropertiesManager;
import io.restassured.response.Response;
import lombok.extern.slf4j.Slf4j;

import static io.restassured.RestAssured.given;

@Slf4j
public class PdsService {

    public String getGtpId(String eventId) {
        log.info("Get GTP Event Id From PDS...");
        Response eventInPdsResponse = getEventInPdsResponse(eventId);
        return getGtpIdFromResponse(eventInPdsResponse);
    }

    private Response getEventInPdsResponse(String eventId) {
        Response response = given()
                .log().uri()
                .get(getEventUrl(eventId));

        if (response.statusCode() == 404) {
            log.info("404 from PDS, recheck data in PDS...");
            response = given()
                    .log().uri()
                    .get(getEventUrl(eventId));
        }

        return response;
    }

    private String getEventUrl(String eventId) {
        return String.format(PropertiesManager.getPdsUrl() + "/events/OB_EV%s?fields=gtpId", eventId);
    }

    private String getGtpIdFromResponse(Response response) {
        try {
            return response.as(EventInPds.class).getGtpId();
        } catch (NullPointerException e) {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
