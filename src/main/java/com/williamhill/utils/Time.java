package com.williamhill.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class Time {

    public static String getRandomTodayDate() {
        Random random = new Random();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String date = LocalDate.now().format(formatter);
        String randomHour = String.valueOf(random.nextInt(8) + 15);
        String randomMinute = String.valueOf(random.nextInt(49) + 10);
        String randomSecond = String.valueOf(random.nextInt(49) + 10);
        return String.format("%sT%s:%s:%s.000Z", date, randomHour, randomMinute, randomSecond);
    }

}
