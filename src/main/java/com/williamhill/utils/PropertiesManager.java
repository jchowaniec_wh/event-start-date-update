package com.williamhill.utils;

import com.netflix.config.DynamicProperty;

import static java.util.Objects.nonNull;

public class PropertiesManager {

    public static String getOpenBetBackOfficeUrl() {
        return getProperty("openbet.backoffice.endpoint");
    }

    public static String getGtpUrl() {
        return getProperty("gtp.endpoint");
    }

    public static String getPdsUrl() {
        return getProperty("pds.endpoint");
    }

    public static String getOxiFeedUsername() {
        return getProperty("oxi.feed.username");
    }

    public static String getOxiFeedPassword() {
        return getProperty("oxi.feed.password");
    }

    public static String getOxiFeedEndpoint() {
        return getProperty("oxi.feed.endpoint");
    }

    private static String getProperty(String propertyName) {
        return nonNull(System.getProperty(propertyName)) ?
                System.getProperty(propertyName) :
                DynamicProperty.getInstance(propertyName).getString();
    }

}
