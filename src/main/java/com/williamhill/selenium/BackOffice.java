package com.williamhill.selenium;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.williamhill.utils.PropertiesManager;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;

import static com.codeborne.selenide.Selenide.$$;

@Slf4j
public class BackOffice {

    private final String EVENT_SELECTOR = "table td a";

    public BackOffice open() {
        Selenide.open(PropertiesManager.getOpenBetBackOfficeUrl());
        log.info("Login to Open Bet Back Office");
        Selenide.open(PropertiesManager.getOpenBetBackOfficeUrl() + "?action=ADMIN::EV_SEL::GoEvSel");
        return this;
    }

    public List<String> getAllEventIds() {
        log.info("Collecting all event Ids");
        return $$(EVENT_SELECTOR)
                .asFixedIterable()
                .stream()
                .filter(e -> !isBrokenEvent(e))
                .map(e -> e.attr("href"))
                .filter(Objects::nonNull)
                .map(this::getEventIdFromString)
                .toList();
    }

    private boolean isBrokenEvent(SelenideElement event) {
        String eventName = event.text();
        return eventName.contains("GTP") ||
                eventName.contains("WH Auto Event") ||
                eventName.contains("home_") ||
                eventName.contains("Equipo");
    }

    private String getEventIdFromString(String fullString) {
        String prefixToRemove = "javascript:go_ev\\('";
        String suffixToRemove = "'\\);";
        return fullString
                .replaceAll(prefixToRemove, "")
                .replaceAll(suffixToRemove, "");
    }

}
